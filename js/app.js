$(document).ready(function() {
    $('.owl-rating').owlCarousel({
        loop: true,
        margin: 32,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            413: {
                items: 1
            },
            600: {
                items: 2
            },
            768: {
                items: 2
            },
            1024: {
                items: 3
            },
            1025: {
                items: 4
            },
            2000: {
                items: 4
            }

        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],

    })
});

$(document).ready(function() {
    $('.owl-feature').owlCarousel({
        loop: true,
        margin: 32,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            413: {
                items: 1
            },

            600: {
                items: 1
            },
            768: {
                items: 1
            },
            1024: {
                items: 1
            },
            1025: {
                items: 1
            },
            2000: {
                items: 1
            }

        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],

    })
});


$(document).ready(function() {
    $('.owl-new').owlCarousel({
        loop: true,
        margin: 32,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            413: {
                items: 1
            },
            600: {
                items: 2
            },
            768: {
                items: 2
            },
            1024: {
                items: 3
            },
            1025: {
                items: 4
            },
            2000: {
                items: 4
            }

        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],

    })
});
$(document).ready(function() {
    $('.owl-rating').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            413: {
                items: 1
            },
            600: {
                items: 3
            },
            768: {
                items: 4
            },
            1000: {
                items: 4
            },
            2000: {
                items: 4
            }

        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],

    })
});
$(document).ready(function() {
    $('.owl-selling').owlCarousel({
        loop: true,
        margin: 32,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            413: {
                items: 1
            },
            600: {
                items: 2
            },
            768: {
                items: 2
            },
            1024: {
                items: 3
            },
            1025: {
                items: 4
            },
            2000: {
                items: 4
            }

        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],

    })
});

$(document).ready(function() {
    $('.owl-search-cate').owlCarousel({
        loop: true,
        margin: 16,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 2
            },

            600: {
                items: 2
            },
            768: {
                items: 3
            },
            1000: {
                items: 6
            },
            2000: {
                items: 6
            }

        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],

    })
});

$(document).ready(function() {
    $('.owl-post').owlCarousel({
        loop: true,
        margin: 16,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            413: {
                items: 1
            },
            600: {
                items: 2
            },
            768: {
                items: 3
            },
            1000: {
                items: 4
            },
            2000: {
                items: 4
            }

        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],

    })
});

$(document).ready(function() {
    $("#wrapper #header .menu-mobile ul.main-menu li").click(function() {
        $('#wrapper #header .menu-mobile li>ul').fadeToggle(1000)
        $('#wrapper #header .menu-mobile ul.main-menu li>i').toggleClass('fa-chevron-up')
    });
});

$(document).ready(function() {
    if ($(window).width() <= 1024) {
        $('.input-search').click(function() {
            $('.menu-search').css({ display: 'block', });
        });
    }
});


$(document).ready(function() {
    $('.btn-close').click(function() {
        $('.menu-search').css({ display: 'none', });
    });
});



$(document).ready(function() {
    $('.menu-search .loai-nha-dat:first-child() .select').click(function() {

    });
});












// $(document).ready(function() {
//     $('.product').owlCarousel({
//         loop: true,
//         margin: 10,
//         nav: true,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             600: {
//                 items: 1
//             },
//             768: {
//                 items: 1
//             },
//             1000: {
//                 items: 1
//             },
//             2000: {
//                 items: 1
//             }

//         },

//         // navText: ["<i class='fas fa-angle-double-right'></i>", "<i class='fas fa-angle-double-left'></i>"],

//     })
// });
// $(document).ready(function() {
//     $('.comment').owlCarousel({
//         loop: true,
//         margin: 10,
//         nav: true,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             600: {
//                 items: 2
//             },
//             768: {
//                 items: 2
//             },
//             1000: {
//                 items: 2
//             },
//             2000: {
//                 items: 2
//             }


//         },

//         // navText: ["<i class='fas fa-angle-double-right'></i>", "<i class='fas fa-angle-double-left'></i>"],

//     })
// });


// $(document).ready(function() {
//     $('.noi-bat').owlCarousel({
//         loop: true,
//         margin: 10,
//         nav: true,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 2
//             },
//             400: {
//                 items: 2
//             },

//             600: {
//                 items: 2
//             },
//             768: {
//                 items: 3
//             },
//             1000: {
//                 items: 4
//             },
//             1366: {
//                 items: 3
//             }


//         },

//         navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],

//     })
// });
// $(document).ready(function() {
//     $('.owl-sapo').owlCarousel({
//         loop: true,
//         margin: 10,
//         nav: true,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 0
//             },
//             300: {
//                 items: 2
//             },
//             400: {
//                 items: 2
//             },
//             600: {
//                 items: 2
//             },
//             768: {
//                 items: 3
//             },
//             1000: {
//                 items: 5
//             },
//             2000: {
//                 items: 5
//             }


//         },

//         // navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],

//     })
// });
// $(document).ready(function() {
//     $('.owl-hotline-email-time').owlCarousel({
//         loop: true,
//         margin: 10,
//         nav: true,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             600: {
//                 items: 2
//             },
//             768: {
//                 items: 2
//             },
//             1000: {
//                 items: 3
//             },
//             2000: {
//                 items: 3
//             }


//         },

//         // navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],

//     })
// });

// $(document).ready(function() {
//     $('.owl-linh-vuc').owlCarousel({
//         loop: true,
//         margin: 10,
//         nav: true,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             600: {
//                 items: 2
//             },
//             768: {
//                 items: 2
//             },
//             1000: {
//                 items: 3
//             },
//             2000: {
//                 items: 3
//             }
//         },
//         // navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],
//     })
// });

// $(document).ready(function() {
//     $('.owl-selling-products').owlCarousel({
//         loop: true,
//         margin: 10,
//         nav: true,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             600: {
//                 items: 1
//             },
//             768: {
//                 items: 1
//             },
//             1000: {
//                 items: 1
//             },
//             2000: {
//                 items: 1
//             }
//         },
//         navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],
//     })
// });


// $(document).ready(function() {

//     $(window).scroll(function() {

//         //khoảng cách từ scropll tới top
//         var window_croll = $(window).scrollTop();
//         // console.log('a = ' + window_croll);

//         //khoảng cách từ slider tới top bằng height của header
//         // var slider_top = $('.slider').offset().top;
//         // console.log('b = ' + slider_top);

//         //khi vị trí thanh cuộn lớn hơn hoặc bằng slider-top thì đổi màu
//         if (window_croll >= 100) {
//             $('.go-to-top').css('display', 'block');
//         } else {
//             $('.go-to-top').css('display', 'none');
//         }
//     });

// })


// $(document).ready(function() {
//     $('.navbar-light .navbar-toggler').click(function() {
//         $('.navbar ').css('position', 'relative');
//         $('.search').css({ position: 'absolute', top: '40px', right: '0px' });
//         $('.search span').css({ position: 'absolute', top: '-14px' });
//         $('.navbar-brand').css({ position: 'absolute', top: '25px' });
//     })

// })



// $(document).ready(function() {
//     // $(window).resize(function() {
//     // nếu màn hình có độ rộng lớn hơn 480px thì menu-footer ẩn đi


//     if ($(window).width() <= 480) {

//         // $('.about-us ul').css('display', 'block');

//         $(".about-us .block-2").click(function() {
//             $(".about-us .block-2 ul").slideToggle(500);
//             $('.about-us .block-2 .icon-block-2').toggleClass('fa-chevron-up')
//         });

//         $(".about-us .block-3").click(function() {
//             $(".about-us .block-3 ul").slideToggle(500);
//             $('.about-us .block-3 .icon-block-3').toggleClass('fa-chevron-up')
//         });
//     }

// })


// $(document).ready(function() {
//     // $('.main-news .menu-sidebar-left ul.list-menu .product-menu').click(function() {
//     //     $('.main-news .menu-sidebar-left ul.list-product ').css('display', 'block');
//     //     $(".main-news .menu-sidebar-left .product-menu ").slideToggle(1000);
//     //     unset();
//     //     // $('.main-news .menu-sidebar-left ul.list-product ').css('display', 'block');
//     // });

//     $(".main-news .menu-sidebar-left ul.list-menu .fa-chevron-down").click(function() {

//         $(".main-news .menu-sidebar-left ul.list-product").toggle(500);
//         $(".main-news .menu-sidebar-left .fa-chevron-down").toggleClass('fa-chevron-up');
//     });
// })


// $(document).ready(function() {

//     $('.main-news .thong-tin .menu-sidebar-left ul.list-menu .see-more ').click(function() {
//         $('.main-news .thong-tin .menu-sidebar-left ul.list-menu .see-more').css('display', 'none');
//         $('.main-news .thong-tin .menu-sidebar-left ul.list-menu .more').slideDown(500);
//     })

//     $('.main-news .thong-tin .menu-sidebar-left ul.list-menu .thu-gon').click(function() {
//         $('.main-news .thong-tin .menu-sidebar-left ul.list-menu .more').slideUp(500);
//         $('.main-news .thong-tin .menu-sidebar-left ul.list-menu .see-more').css('display', 'block');
//     })

// })


// $(document).ready(function() {


//     $('#wrapper #header .header-2 .form-inline .fa-search').hover(
//         function() {
//             $('#wrapper #header .header-2 .form-inline .search .input-search').css('display', 'block')
//             $('#wrapper #header .header-2 .form-inline .search .btn-tim-kiem').css('display', 'block')
//         },

//         // function() {
//         //     $('#wrapper #header .header-2 .form-inline .search .input-search').css('display', 'block')
//         //     $('#wrapper #header .header-2 .form-inline .search .btn-tim-kiem').css('display', 'block')
//         // }
//     );

//     $('#wrapper #header .header-2 .form-inline .search .input-search').hover(
//         function() {
//             $('#wrapper #header .header-2 .form-inline .search .input-search').css('display', 'block')
//             $('#wrapper #header .header-2 .form-inline .search .btn-tim-kiem').css('display', 'block')
//         },
//         function() {
//             $('#wrapper #header .header-2 .form-inline .search .input-search').css('display', 'none')
//             $('#wrapper #header .header-2 .form-inline .search .btn-tim-kiem').css('display', 'none')
//         }
//     );
//     $('#wrapper #header .header-2 .form-inline .search .btn-tim-kiem').hover(
//         function() {
//             $('#wrapper #header .header-2 .form-inline .search .input-search').css('display', 'block')
//             $('#wrapper #header .header-2 .form-inline .search .btn-tim-kiem').css('display', 'block')
//         },
//         function() {
//             $('#wrapper #header .header-2 .form-inline .search .input-search').css('display', 'none')
//             $('#wrapper #header .header-2 .form-inline .search .btn-tim-kiem').css('display', 'none')
//         }
//     );



//     $(".header-2 .search .fa-shopping-bag").hover(

//         function() {
//             $('#wrapper .header-2 .hidden-cart').css('display', 'block');
//         },
//         // // hover out
//         // function() {
//         //     $('#wrapper .header-2 .hidden-cart').css('display', 'none');
//         // }
//     );

//     $("#wrapper .header-2 .hidden-cart").hover(
//         //hover in
//         function() {
//             $('#wrapper .header-2 .hidden-cart').css('display', 'block');
//         },

//         // hover out
//         function() {
//             $('#wrapper .header-2 .hidden-cart').css('display', 'none');
//         }

//     );

// })


// $(document).ready(function() {
//     if ($(window).width() <= 768) {

//         $('.main-check-out .order-list').css('display', 'none');
//         $('.main-check-out .fee').css('display', 'none');
//         $('.main-check-out .total').css('display', 'none');
//         $('.main-check-out .back').css('display', 'none');
//         $('.main-check-out h1.shop-name').css('display', 'none');
//     }
// })

// $(document).ready(function() {

//     $('.main-check-out .order-info').click(function() {

//         $('.main-check-out .order-list').slideToggle(500);
//         $('.main-check-out .fee').slideToggle(500);
//         $('.main-check-out .total').slideToggle(500);
//         // $('.main-check-out .back').slideToggle(500);
//         $('.main-check-out .order-info .fa-chevron-down').toggleClass('fa-chevron-up');
//     })


// })


// $(document).ready(function() {
//     $('.lien-quan').owlCarousel({
//         loop: true,
//         margin: 10,
//         nav: true,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 2
//             },
//             400: {
//                 items: 2
//             },

//             600: {
//                 items: 2
//             },
//             768: {
//                 items: 3
//             },
//             1000: {
//                 items: 4
//             },
//             1366: {
//                 items: 4
//             }


//         },

//         navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],

//     })
// });

// $(document).ready(function() {
//     $("#zoom").elevateZoom({ gallery: 'list-thumb', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'images/detail-product/img-show.png' });
// })


// // xử lí hiển thị ảnh products-detail

// $(document).ready(function() {


//     //hiển thị ảnh mặc định là ảnh đầu tiên của list-img-show
//     var src_img_first;
//     src_img_first = $('.main-products-detail .block-1 ul.list-img-show li:first-child img').attr('src');
//     $(".main-products-detail .block-1 img#zoom").attr('src', src_img_first);

//     //bước 1: lấy src của hình ảnh khi click vào 
//     $(".main-products-detail .block-1 ul.list-img-show li img").click(function() {
//         var src_img;
//         src_img = $(this).attr('src');

//         // bươc 2: gán gia trị src cho ảnh lớn hiển thị
//         $(".main-products-detail .block-1 img#zoom").attr('src', src_img);

//         // addClass khi click vào 
//         $(".main-products-detail .block-1 ul.list-img-show li img").removeClass('add-class-border');
//         // khi click vào img  => xóa class tất cả img  trước => add class cho 1 img click vào sau 
//         $(this).addClass('add-class-border');

//         return false;

//     });
// })


// $(document).ready(function() {
//     $('.owl-tin-tuc-lien-quan').owlCarousel({
//         loop: true,
//         margin: 30,
//         nav: false,
//         dots: false,
//         responsive: {
//             0: {
//                 items: 2
//             },
//             400: {
//                 items: 2
//             },

//             600: {
//                 items: 2
//             },
//             768: {
//                 items: 3
//             },
//             1000: {
//                 items: 3
//             },
//             1366: {
//                 items: 3
//             }


//         },

//         // navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],

//     })
// });


// $(document).ready(function() {


//     if ($(window).width() >= 769) {
//         $("#wrapper .header-2 ul.navbar-nav .dropdown").hover(
//             function() {
//                 $('#wrapper .header-2 ul.navbar-nav .dropdown-menu-full').css('display', 'block');
//             },

//             // function() {
//             //     $('#wrapper .header-2 ul.navbar-nav .dropdown-menu-full').css('display', 'none');
//             // },
//         );
//     }

//     $("#wrapper .header-2 ul.navbar-nav .dropdown-menu-full").hover(

//         function() {
//             $('#wrapper .header-2 ul.navbar-nav .dropdown-menu-full').stop().slideDown(500);
//         },

//         function() {
//             $('#wrapper .header-2 ul.navbar-nav .dropdown-menu-full').stop().slideUp(500);
//         },
//     );

// })

$(document).ready(function() {
    $("#muc-gia").click(function() {
        $("#box-muc-gia").slideToggle(1000);

    });
})

$(document).ready(function() {
    $("#dien-tich").click(function() {
        $("#box-dien-tich").slideToggle(1000);

    });
})

$(document).ready(function() {
    $("#wrapper-content .form-search-bot  #xem-them").click(function() {
        $("#wrapper-content .form-search-bot #box-xem-them").slideToggle(1000);
    });
})



// lọc mức giá 
$(document).ready(function() {
    let min = 0;
    let max = 100;

    const calcLeftPosition = value => 100 / (100 - 0) * (value - 0);

    $('#rangeMin').on('input', function(e) {
        const newValue = parseInt(e.target.value);
        if (newValue > max) return;
        min = newValue;
        $('#thumbMin').css('left', calcLeftPosition(newValue) + '%');
        $('#min').html(newValue);
        $('.muc-gia-change').html('Từ ' + newValue + ' đến ');
        $('.muc-gia-change').css('display', 'inline-block');
        $('#line').css({
            'left': calcLeftPosition(newValue) + '%',
            'right': (100 - calcLeftPosition(max)) + '%'
        });
    });

    $('#rangeMax').on('input', function(e) {
        const newValue = parseInt(e.target.value);
        if (newValue < min) return;
        max = newValue;
        $('#thumbMax').css('left', calcLeftPosition(newValue) + '%');
        $('#max').html(newValue);
        $('.muc-gia-change-2').html(newValue + ' Tỷ');
        $('.muc-gia-change-2').css('display', 'inline-block');
        $('#line').css({
            'left': calcLeftPosition(min) + '%',
            'right': (100 - calcLeftPosition(newValue)) + '%'
        });
    });
})

// lọc diện tích
$(document).ready(function() {
    let min = 0;
    let max = 100;

    const calcLeftPosition = value => 100 / (100 - 0) * (value - 0);

    $('#rangeMin-2').on('input', function(e) {
        const newValue = parseInt(e.target.value);
        if (newValue > max) return;
        min = newValue;
        $('#thumbMin-2').css('left', calcLeftPosition(newValue) + '%');
        $('#min-dien-tich').html(newValue);
        $('.dien-tich-change').html('Từ ' + newValue + ' đến ');
        $('.dien-tich-change').css('display', 'inline-block');
        $('#line-2').css({
            'left': calcLeftPosition(newValue) + '%',
            'right': (100 - calcLeftPosition(max)) + '%'
        });
    });

    $('#rangeMax-2').on('input', function(e) {
        const newValue = parseInt(e.target.value);
        if (newValue < min) return;
        max = newValue;
        $('#thumbMax-2').css('left', calcLeftPosition(newValue) + '%');
        $('#max-dien-tich').html(newValue);
        $('.dien-tich-change-2').html(newValue + ' m2');
        $('.dien-tich-change-2').css('display', 'inline-block');
        $('#line-2').css({
            'left': calcLeftPosition(min) + '%',
            'right': (100 - calcLeftPosition(newValue)) + '%'
        });
    });
})





$(document).ready(function() {
    $(".sort-muc-gia li ").click(function() {
        var price = $(this).text();
        $('.muc-gia-change').html(price);
        $('.muc-gia-change-2').css('display', 'none');
        $('#wrapper-content .search-form .form-search-bot #box-muc-gia').css('display', 'none');
    });
})


$(document).ready(function() {
    $(".sort-dien-tich li ").click(function() {
        var price = $(this).text();
        $('.dien-tich-change').html(price);
        $('.dien-tich-change-2').css('display', 'none');
        $('#wrapper-content .search-form .form-search-bot #box-dien-tich').css('display', 'none');
    });

})